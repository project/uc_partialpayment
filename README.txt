Ubertcart Partial Payment README
--------------------------------

Ubercart Partical Payment is a module designed to allow you to complete checkout and  accept partial 
payments for sales made by  your Drupal/Ubercart website



Setup:
======
  . admin/build/modules: Enable module
  . admin/settings/uc_partialpayment : Set up the module
  . admin/store/settings/payment/edit/methods: Set PayPal cart submission method as "Submit the whole order as a single line item" 
  . admin/store/settings/checkout/edit/panes: Disable standard "Payment method", Enable "Partial Payment method"


Limits:
=======

  . Works only with PayPal
  . Works only with fixed values (Parcial Payment as Order total percentage planned as improvement)
  


Sponsored by <a href='http://noteurbane.com'>http://noteurbane.com</a>


 
Thanks for checking out UC Partial Payment! 
I'd love to hear your feedback.


Augusto Fagioli
augustofagioli@gmail.com